<?php
//Start a new session or retrieve existing one
session_start();

//Require php files
require_once"php/render.php";
require_once"php/permission.php";

//Create objects
$render = new Render();
$permission= new Permission();

//Redirect if already logged in
$permission->checkLogin();

//Render top of page
include ('header.php');
$render->openPage("Login", "");
?>
                   
<div id="login_form">
<div id="login_form_center" class="smallForm">
   <fieldset>
       <legend>Existing User</legend>
       <form name="old_user" action="/handler/loginprocess.php" method="POST">
        <div class="pure-control-group"><label> Email:*</label> <input type="text" data-validation="email"  name="email"value =" <?php if(isset($_SESSION["email"])) echo $_SESSION["email"];?>"></div>
         <div class="pure-control-group"><label>Password:*</label> <input type="password" data-validation="required" name="password"></div>
        <div class="submit-control"><input type="submit" class="submit-button" name="existingButton" value="Login"></div>
        </fieldset>
      </form>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.27/jquery.form-validator.min.js"></script>
<script> $.validate();
</script>

</div>
<div id ="newUser">
<a href="newUserLogin.php">
<h1 id="new">New User</h1>
</a>
</div>

<div id="login_error">
<span class="error-php"><?php if(isset($_SESSION["emailErr"])){echo $_SESSION["emailErr"];}?><span><br>
<span class="error-php"><?php if(isset($_SESSION["passErr"])){echo $_SESSION["passErr"];}?><span>
</div>
</div>

<?php 
$_SESSION["emailErr"]="";
$_SESSION["passErr"]="";
include ('footer.php');?>
