<?php 
//Get session information
session_start();

//Require files
require_once"php/render.php";
require_once"php/permission.php";
require_once"php/dao.php";

//Creat Object
$permission = new Permission();
$render = new Render();
$dao = new Dao();

//Render top of page
$permission->userOnly();
$signout = (isset($_SESSION["permission"])) ?"Sign out": "";
include('header.php');
$render->openPage( "Information",$signout);
 $menuOptions =array(
   "Information"=>"information.php",
   "About"=>"user_about.php",
   "Results"=>"user_results.php");
if($_SESSION["permission"] ==1)
   $menuOptions[Admin]="admin.php";
$render->sideMenuArray($menuOptions);  
$user = $dao->getUser($_SESSION["id"]);

?>
   <div id="athlete_profile">
      <form  name="athlete_info" action="handler/userinfo.php" method = "POST">
            <input type="hidden" name="check_submit" value="1" />
         <fieldset>
            <legend>Contact Information</legend>
            <div class ="pure-control-group"><label for="First_Name">First Name:</label> <input type="text" data-validation="alphanumeric" name="First_Name" value="<?php echo $user["first_name"];?>"></div>
               <div class ="pure-control-group"><label for="Last_Name">Last Name:</label> <input type="text" data-validation="alphanumeric" name="Last_Name"value="<?php echo $user["last_name"];?>"></div>
               <div class ="pure-control-group"><label for="telephone">Telephone:</label> <input type="tel" data-validation="alphanumeric" name="telephone"value="<?php if(isset($_SESSION["telephone"])) echo $_SESSION["telephone"];else  echo $user["phone"];?>"></div>
               <div class ="pure-control-group"><label for="Email">Email: </label><input type="email" name="email" data-validation="email" value="<?php echo $user["email"];?>"></div>

               <div class ="pure-control-group"><label for="Street_Address">Street Address </label><input type="text" data-validation="alphanumberic" name="Street"value="<?php if(isset($_SESSION["address"])) echo $_SESSION["address"]; else echo $user["street"];?>"></div>
               <div class ="pure-control-group"><label for="City">City: </label><input type="text" data-validation="alphanumeric" name="City"value="<?php if(isset($_SESSION["city"])) echo $_SESSION["city"]; else echo $user["city"];?>"></div>
               <div class ="pure-control-group"><label for="State">State: </label><input type="text" name="State" data-validation="federatestate"  size="2"value="<?php if(isset($_SESSION["state"])) echo $_SESSION["state"]; else echo $user["state"];?>"></div>
               <div class ="pure-control-group"><label for="Zip_Code">Zip code: </label><input type="text" data-validation="number length" data-validation-length ="5-5" name="Zip" size="5"value="<?php if(isset($_SESSION["zip"])) echo $_SESSION["zip"]; else echo $user["zipcode"];?>"></div>
               <div class ="pure-control-group"><label for="Country">Country: </label><input type="text" data-validation="country"  name="Country"value="<?php if(isset($_SESSION["country"])) echo $_SESSION["country"]; else echo $user["country"];?>"></div>
         </fieldset>
         <fieldset>
            <legend>Race Information</legend>

              <div class="top-padding"> Gender: </div>
               <div class ="radio-control"><label ><input type="radio" name="sex" value="F"<?php if($_SESSION["sex"] =="F")echo"checked"; if($user["gender"] =="F")echo "checked";?>>Female</label></div>
               <div class ="radio-control"><label><input type="radio" name="sex" value="M"<?php if($_SESSION["sex"] =="M")echo"checked";  if($user["gender"] =="M")echo "checked";?>>Male</label></div>
              <div class="top-padding">Shirts size:</br></div>
               <div class ="radio-control"> <label><input type="radio" name="shirt_size" value="xxs"<?php if($_SESSION["shirt"]== "xxs"|| $user["shirt_size"] =="xxs")echo "checked";?>>XXS</label></br></div>
         <div class ="radio-control"><label><input type="radio" name="shirt_size" value="xs"<?php if($_SESSION["shirt"]== "xs"|| $user["shirt_size"] =="xs")echo "checked";?>>XS</label></br></div> 
         <div class ="radio-control"><label><input type="radio" name="shirt_size" value="s"<?php if($_SESSION["shirt"]== "s"|| $user["shirt_size"] =="s")echo "checked";?>>S</label></br> </div> 
         <div class ="radio-control"><label><input type="radio" name="shirt_size" value="m"<?php if($_SESSION["shirt"]== "m"|| $user["shirt_size"] =="m")echo "checked";?>>M</label></br> </div> 
         <div class ="radio-control"><label><input type="radio" name="shirt_size" value="l"<?php if($_SESSION["shirt"]== "l"|| $user["shirt_size"] =="l")echo "checked";?>>L</label></br> </div> 
         <div class ="radio-control"><label><input type="radio" name="shirt_size" value="xl"<?php if($_SESSION["shirt"]== "xl"|| $user["shirt_size"] =="xl")echo "checked";?>>XL</label></br> </div> 
         <div class ="radio-control"><label><input type="radio" name="shirt_size" value="xxl"<?php if($_SESSION["shirt"]== "xxl"|| $user["shirt_size"] =="xxl")echo "checked";?>>XXL</label></br></li> </div> 
          <div class="top-padding">Birthday:</br></div>
          <div class ="radio-control"><input type="date" name="birthday" value="<?php if(isset($_SESSION["birthday"]))echo $_SESSION["birthday"]; else echo $user["date_of_birth"];?>"></div>
         </fieldset>
         <fieldset>
            <legend>Emergency Contact:</legend>
               <div class ="pure-control-group"><label for="emergency_name">Name:</label><input type="text" data-validation="custom" data-validation-regexp="^([a-zA-Z ]+$)" name="emergency_name"value="<?php if(isset($_SESSION["emergency_name"])) echo $_SESSION["emergency_name"]; else echo $user["emergency_contact_name"];?>"></div>
               <div class ="pure-control-group"><label for="emergency_telephone">Telephone:</label> <input type="tel"  data-validation="number length" data-validation-length="10-11" name="emergency_telephone"value="<?php if(isset($_SESSION["emergency_telephone"])) echo $_SESSION["emergency_telephone"]; else echo $user["emergency_phone"];?>"></div>
               <div class ="top-padding">Relationship:</div> 
               <div class ="radio-control"><label><input type="radio" name="emergency_relationship" value="parent"<?php if($user["emergency_relation"] =="parent" || $_SESSION["relationship"]=="parent" )echo "checked";?>>Parent</label></div>
               <div class ="radio-control"><label><input type="radio" name="emergency_relationship" value="spouse"<?php if($user["emergency_relation"] =="spouse" ||$_SESSION["relationship"]=="spouse")echo "checked";?>>Spouse</label></div>
               <div class ="radio-control"><label><input type="radio" name="emergency_relationship" value="relative"<?php if($user["emergency_relation"] =="relative" ||$_SESSION["relationship"]=="relative")echo "checked";?>>Relative</label></div>
               <div class ="radio-control"><label><input type="radio" name="emergency_relationship" value="friend"<?php if($user["emergency_relation"] =="friend"|| $_SESSION["relationship"]=="friend")echo "checked";?>>Friend</label></div>
            <div class="submit-control"><input class="submit-button" name ="save_info" type="submit" value="Save"/></div>
         </fieldset>
      </form>
<script>
$(document).ready(function(){
   var options = {
      clearForm: true,
      resetForm: true
   };
   $('#athlete_profile').ajaxForm();
});
</script>
   </div>
 <div id="login_error">
   <span class="error-php"><?php if(isset($_SESSION["firstnameErr"])){echo$_SESSION["firstnameErr"];}?></span><br> 
   <span class="error-php"><?php if(isset($_SESSION["lastnameErr"])){echo$_SESSION["lastnameErr"];}?></span><br> 
   <span class="error-php"><?php if(isset($_SESSION["telephoneErr"])){echo$_SESSION["telephoneErr"];}?></span><br> 
  <span class="error-php"><?php if(isset($_SESSION["addressErr"])){echo$_SESSION["addressErr"];}?></span><br> 
  <span class="error-php"><?php if(isset($_SESSION["cityErr"])){echo$_SESSION["cityErr"];}?></span><br> 
 <span class="error-php"><?php if(isset($_SESSION["stateErr"])){echo$_SESSION["stateErr"];}?></span><br> 
 <span class="error-php"><?php if(isset($_SESSION["zipErr"])){echo$_SESSION["zipErr"];}?></span><br>                  
 <span class="error-php"><?php if(isset($_SESSION["countryErr"])){echo$_SESSION["countryErr"];}?></span><br>              
 <span class="error-php"><?php if(isset($_SESSION["genderErr"])){echo$_SESSION["genderErr"];}?></span><br>          
 <span class="error-php"><?php if(isset($_SESSION["shirtErr"])){echo$_SESSION["shirtErr"];}?></span><br>      
 <span class="error-php"><?php if(isset($_SESSION["birthdayErr"])){echo$_SESSION["birthdayErr"];}?></span><br>  
 <span class="error-php"><?php if(isset($_SESSION["emergency_nameErr"])){echo$_SESSION["emergency_nameErr"];}?></span><br> 
 <span class="error-php"><?php if(isset($_SESSION["emergency_telephoneErr"])){echo$_SESSION["emergency_telephoneErr"];}?> </span><br> 
 <span class="error-php"><?php if(isset($_SESSION["relationshipErr"])){echo$_SESSION["relationshipErr"];}?></span><br> 
</div>
<script src="http://malsup.github.com/jquery.form.js"></script> 
<?php include ('footer.php');?>
