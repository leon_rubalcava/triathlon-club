<?php 

class Permission{

 public function checkLogin(){
    if(isset($_SESSION["permission"]))
       header("Location: user_home.php");
 }
 public function adminOnly(){
       if($_SESSION["permission"] != 1)
          header("Location: login.php");
 }
 public function userOnly(){
       if(!isset($_SESSION["permission"]))
          header("Location: login.php");
 }

}
