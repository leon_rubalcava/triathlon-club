<?php

class Sanitize{
   
   public function cleanInput($str){
       $str = trim($str);
       $str = stripslashes($str);
       $str = htmlspecialchars($str);
       return $str ;
    }
 
}
