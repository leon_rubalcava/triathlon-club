<?php 

class Dao{
	private $host = "localhost";
	private $db = "tri_club_web";
	private $login= "pubert";
	private $pass= "123Qapple";
	private $salt= "ab8cd";


	public function getConnection(){
		return new PDO("mysql:host={$this->host};dbname={$this->db}", $this->login, $this->pass);
		//return new PDO("mysql:host=localhost;dbname=tri_club_web", "pubert", "123Qapple");
	}
	public function getSalty($toBeSalted){
		return sha1($toBeSalted . $this->salt);
	}
	
	public function createUser($firstName, $lastName, $email, $password, $permission){
		$conn = $this->getConnection();
		$hashed = $this->getSalty($password); 
		//$regularexp = /\w*\@(boisestate|u.boisestate)+(.edu)/;
      $saveUser = "INSERT INTO athlete(first_name, last_name, email, password,permissions) 
                  VALUES(:firstName,:lastName,:email,:password,:permissions)";
		$newVar = $conn->prepare($saveUser);
      if(!$newVar){
         print_r($conn->errorInfo());}
		$newVar->bindParam(":firstName", $firstName);
		$newVar->bindParam(":lastName", $lastName);
		$newVar->bindParam(":email", $email);
		$newVar->bindParam(":permissions", $permission);
      //change back to pass
		$newVar->bindParam(":password", $hashed);
		$newVar->execute();

	}

	public function hasPermission( $email, $password){
		$conn = $this->getConnection();
		$hashed =$this->getSalty($password);
		$getUser = "SELECT id, first_name, permissions FROM athlete WHERE email = :email AND password =:password";
		$newVar = $conn->prepare($getUser);
		$newVar->bindParam(":email", $email);
		$newVar->bindParam(":password", $hashed);
		$newVar->execute();
			return reset($newVar->fetchAll());
	}


   public function userExists( $email ){
		$conn = $this->getConnection();
		$getUser = "SELECT  email FROM athlete WHERE email = :email ";
		$newVar = $conn->prepare($getUser);
		$newVar->bindParam(":email", $email);
		$newVar->execute();
      $retEmail = reset($newVar->fetchAll());
			return $retEmail['email'];
	}



   public function getUser($idNum){
      $conn=$this->getConnection();
      $getUser ="SELECT * FROM athlete WHERE id =" . $idNum;
      $prep = $conn->prepare($getUser);
      $prep->bindParam(":id", $idNum);
      $prep->execute();
         return $prep->fetch();
   }


   public function saveAbout($id, $article){
		$conn = $this->getConnection();
      $saveUser = "UPDATE athlete SET about=:about WHERE id=:id";                                                             
		$newVar = $conn->prepare($saveUser);
		$newVar->bindParam(":about", $article);
		$newVar->bindParam(":id", $id);
		$newVar->execute();
	}

   public function userProfile($id, $firstName, $lastName, $email, $telephone, 
      $street,$city,$state,$zip,$country,$gender,$shirt,$birthday,$emergencyName,
      $emergencyPhone, $relationship){
		$conn = $this->getConnection();

      $saveUser = "UPDATE athlete SET  first_name=:firstName, last_name=:lastName, country=:country, street=:street, zipcode=:zipcode, city=:city,
         state=:state,phone=:phone, email=:email,emergency_contact_name=:emergency_contact_name, emergency_phone=:emergency_phone, emergency_relation=:emergency_relation,
         shirt_size=:shirt_size, date_of_birth=:date_of_birth, gender=:gender WHERE id=:id";                                                             
      
		$newVar = $conn->prepare($saveUser);
		$newVar->bindParam(":firstName", $firstName);
		$newVar->bindParam(":lastName", $lastName);
		$newVar->bindParam(":country", $country);
		$newVar->bindParam(":street", $street);
		$newVar->bindParam(":zipcode", $zip);
		$newVar->bindParam(":city", $city);
		$newVar->bindParam(":state", $state);
		$newVar->bindParam(":phone", $telephone);
		$newVar->bindParam(":emergency_contact_name", $emergencyName);
		$newVar->bindParam(":emergency_phone", $emergencyPhone);
		$newVar->bindParam(":emergency_relation", $relationship);
		$newVar->bindParam(":shirt_size", $shirt);
		$newVar->bindParam(":date_of_birth", $birthday);
		$newVar->bindParam(":gender", $gender);
		$newVar->bindParam(":email", $email);
		$newVar->bindParam(":id", $id);
		$newVar->execute();
	}

   public function saveArticle($title, $article, $type){
      $conn = $this->getConnection();
      if($type =="training_article")
      $query= "INSERT INTO training_article (title, article) VALUES (:title, :article)";
      if($type =="nutrition_article")
      $query="INSERT INTO nutrition_article (title, article) VALUES (:title, :article)";
      $prep = $conn->prepare($query);
      $prep->bindParam(":title", $title);
      $prep->bindParam(":article", $article);
      $prep->execute();
   }
   public function articleTitles($type){
      $conn = $this->getConnection();
      if($type =="training_article")
		$getTitle = "SELECT title, id FROM training_article";
      if($type =="nutrition_article")
		$getTitle = "SELECT  title,id FROM nutrition_article";
			return $conn->query($getTitle);
  }
   public function getArt($type,$id){
      $conn = $this->getConnection();
      if($type =="training_article")
		$getTitle= "SELECT article, title FROM training_article WHERE id= :id";
      if($type =="nutrition_article")
		$getTitle= "SELECT  article, title FROM nutrition_article WHERE id= :id";
      $prep = $conn->prepare($getTitle);
      $prep->bindParam(":id", $id);
      $prep->execute();
			return reset($prep->fetchAll());
  }
    public function getAbout(){
      $conn = $this->getConnection();
		$getArt= "SELECT first_name, about  FROM athlete WHERE about != null";
			return $conn->query("SELECT first_name, about  FROM athlete WHERE about != \"NULL\"");
  }


    public function getRaces(){
      $conn = $this->getConnection();
      $getRaces="SELECT race_results.run_distance, 
         race_results.run_time, 
         race_results.bike_distance, 
         race_results.bike_time, 
         race_results.swim_distance, 
         race_results.swim_time,  
         race_results.race_name,  
         athlete.first_name 
         FROM race_results 
         INNER JOIN athlete 
         ON race_results.athlete_id=athlete.id";
			return $conn->query($getRaces);
  }
 
  public function saveWorkout($id, $time, $distance, $type){
      $conn = $this->getConnection();
      if($type =="bike")
      $query= "INSERT INTO bike_workout (mile_distance, mile_time, athlete_id) VALUES (:distance, :time, :id )";
      if($type =="run")
      $query="INSERT INTO run_workout (mile_distance, mile_time, athlete_id) VALUES (:distance, :time, :id)";
      if($type =="swim")
      $query="INSERT INTO swim_workout (meters, mile_time, athlete_id) VALUES (:distance,  :time, :id)";
      $prep = $conn->prepare($query);
      $prep->bindParam(":id", $id);
      $prep->bindParam(":time", $time);
      $prep->bindParam(":distance", $distance);
      $prep->execute();
   }

  public function saveResult($id, $runtime, $rundistance, $biketime, $bikedistance, $swimtime, $swimdistance, $racename){
      $conn = $this->getConnection();
     $query= "INSERT INTO race_results (athlete_id, run_distance, run_time, bike_distance, bike_time, swim_distance, swim_time, race_name)
                             VALUES (:id, :runtime, :rundistance, :biketime, :bikedistance, :swimtime,:swimdistance, :racename)";
      $prep = $conn->prepare($query);
      $prep->bindParam(":id", $id);
      $prep->bindParam(":runtime", $runtime);
      $prep->bindParam(":rundistance", $rundistance);
      $prep->bindParam(":biketime", $biketime);
      $prep->bindParam(":bikedistance", $bikedistance);
      $prep->bindParam(":swimtime", $swimtime);
      $prep->bindParam(":swimdistance", $swimdistance);
      $prep->bindParam(":racename", $racename);
      $prep->execute(); 
   }
}


?>
