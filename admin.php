<?php 
//Get session information
session_start();

//Require files
require_once"php/render.php";
require_once"php/permission.php";

//Creat Object
$permission = new Permission();
$render = new Render();

$permission->adminOnly();
//Render top of page

$signout = (isset($_SESSION["permission"])) ?"Sign out": "";
include('header.php');
$render->openPage("Admin",$signout);
 /*
 *Menu items are in an associative array of the form
 *$displayedText => Url
 *
 */
$menuOptions =array(
   "Information"=>"information.php",
   "About"=>"user_about.php",
   "Results"=>"user_results.php");
if($_SESSION["permission"] ==1)
   $menuOptions[Admin]="admin.php";
$render->sideMenuArray($menuOptions);  

// populate wither user name
?>

<div id="Event" class="athlete_profile">
<form  name="upcomingEvent">
<!--<form  name="upcomingEvent" action="/handler/newProcess.php" method = "POST">-->
               <fieldset>

                  <legend>Next Event</legend>
    <div class ="pure-control-group"><label for="Event_Name">Event Name:</label><input type="text" data-validation="custom " data-validation-regexp="^([a-zA-Z ]+$)"name="event">
     <div class ="pure-control-group"><label for="Location"> Location:</label><input type="text" data-validation="custom " data-validation-regexp="^([a-zA-Z ]+$)"name="location">
     <div class ="pure-control-group"><label for="Date">   Date:</label><input type="date" data-validation="date" name="date">
                     <input type="submit" class = "submit-button" name="upcomingEvent" value="submit"/>
</fieldset>
</form>
 <script>
$(document).ready(function(){
   var options = {
      clearForm: true,
      resetForm: true
   };
   $('#Event').ajaxForm(options);
});  
</script>
</div>

<div id="nutrition"class="athlete_profile">  
 <form  name="nutrition" action="/handler/newProcess.php" method = "POST">
 <!--<form  name="nutrition">-->
               <fieldset>
                  <legend>Nutrition Article</legend>
      <div class ="pure-control-group"><label for="Title">Title:</label><input type="text" data-validation="custom " data-validation-regexp="^([a-zA-Z ]+$)"name="Title"></div>
                  <input type="hidden" name="check_submit" value="nutrition_article" />
                  <textarea rows="5" cols="80" data-validation="length" data-validation-length="min100" name = "Article"></textarea>
                  <input type="submit" id="submit" name="nutrition" class ="submit-button" value="submit"/>
</fieldset>
</form>
<script>
$(document).ready(function(){
   var options = {
      clearForm: true,
      resetForm: true
   };
   $('#nutrition').ajaxForm(options);
});
</script>
</div> 
<div id="training"class="athlete_profile">  
 <form  name="training" action="/handler/newProcess.php" method = "POST">
               <fieldset>
                  <legend>Training Article</legend>
       <div class ="pure-control-group"><label for="Title">Title:<input type="text" data-validation="custom " data-validation-regexp="^([a-zA-Z ]+$)"name="Title">
                  <input type="hidden" name="check_submit" value="training_article" />
                  <textarea rows="5" cols="80" data-validation="length" data-validation-length="min100" name="Article"></textarea>
                  <input type="submit" name="training" class = "submit-button"value="submit"/>
</fieldset>
</form>
 <script>
$(document).ready(function(){
   var options = {
      clearForm: true,
      resetForm: true,

   };
   $('#training').ajaxForm(options);
});  
</script>
 </div> 
   <div id="clear">  </div>
<script src="http://malsup.github.com/jquery.form.js"></script> 
<?php  include ('footer.php');?>
