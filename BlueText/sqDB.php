<?php
   class DynamicDb extends SQLite3
   {
      function __construct($name)
      {
         $this->open('dynamic' . $name . '.db');
      }

      public function getAllcontacts($token){
         $connect= $this->open('dynamic' . $token. '.db');
         $result= $connect->query('SELECT * FROM contacts');
         return $result;
      }
   }
