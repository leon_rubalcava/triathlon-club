<?php 
//Gett session data
session_start();

//Require Files
require_once"php/permission.php";
require_once"php/render.php";

//Objects
$render = new Render();
$permission= new Permission();

//Check user permissions
$permission->userOnly();

//Render page
$signout = (isset($_SESSION["permission"])) ?"Sign out": "";
include ('header.php');
$render->openPage("Hello " .  ucfirst($_SESSION["name"]), $signout);

/*
 *Menu items are in an associative array of the form
 *$displayedText => Url
 *
 */
$menuOptions =array(
   "Information"=>"information.php",
   "About"=>"user_about.php",
   "Results"=>"user_results.php");
if($_SESSION["permission"] ==1)
   $menuOptions[Admin]="admin.php";
$render->sideMenuArray($menuOptions); 
?>

<div id="blogContent">

<h1 class=blogTitle> Enter your last workout</h1> 
<div class="padding">
<div id="run" class="workout">
 <form  name="run"  action="handler/newProcess.php" method = "POST">
 <fieldset>
 <legend>Run Workout</legend>
                  <input type="hidden" name="workout" value="run" />
 <div class ="pure-control-group"><label for="distance">Distance:</label> <input type="text" data-validation="number" data-validation-allowing="float" name="distance"></div>
  <div class ="pure-control-group"><label for="run_time">Time:</label> <input type="text" data-validation="number" data-validation-allowing="float"name="time"></div>
                  <input type="submit" class ="submit-button" name="run_workout" value="Save"/>
</fieldset>
</form>

</div>
 

<div id="bike" class="workout">
 <form  name="bike"  action="handler/newProcess.php" method = "POST">
 <fieldset>
 <legend>Bike Workout</legend>
                  <input type="hidden" name="workout" value="bike" />
 <div class ="pure-control-group"><label for="bike_distance">Distance:</label> <input type="text" data-validation="number" data-validation-allowing="float"name="distance"></div>
  <div class ="pure-control-group"><label for="bike_time">Time:</label> <input type="text" data-validation="number" data-validation-allowing="float"name="time"></div>
                  <input type="submit" class ="submit-button" name="bike_workout" value="Save"/>
</fieldset>
</form>
</div>


<div id="swim" class="workout">
 <form  name="swim"  action="handler/newProcess.php" method = "POST">
 <fieldset>
 <legend>Swim Workout</legend>
                  <input type="hidden" name="workout" value="swim" />
 <div class ="pure-control-group"><label for="swim_distance">Distance:</label> <input type="text" data-validation="number" data-validation-allowing="float"name="distance"></div>
  <div class ="pure-control-group"><label for="swim_time">Time:</label> <input type="text" data-validation="number" data-validation-allowing="float"name="time"></div>
                  <input type="submit" class ="submit-button" name="swim_workout" value="Save"/>
</fieldset>
</form>
</div>
</div>
<div id="success" class="error">
<?php echo $_SESSION["success"] ?>
<?php echo $_SESSION["error"]   ?>
</div>

</div>  
 <div id="clear"></div>



<?php
 $_SESSION["success"]="";
 $_SESSION["error"]="";  
 include ('footer.php');?>
