<!DOCTYPE html>

<html>
<head>
<title>Boise State Triathlon Club</title>
<link rel="stylesheet" type="text/css" href="/css/home.css">
<link rel="stylesheet" type="text/css" href="/css/form.css">
<link rel="stylesheet" type="text/css" href="/css/sideMenu.css">
<link rel="stylesheet" type="text/css" href="/css/splash.css">
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="./javascript/jquery-2.0.3.js" type="text/javascript"></script>
<link rel="shortcut icon" href="images/BikeFavicon.png" type="image/x-icon">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.27/jquery.form-validator.min.js"></script>
<link  href="http://fotorama.s3.amazonaws.com/4.4.8/fotorama.css" rel="stylesheet"> 
<script src="http://fotorama.s3.amazonaws.com/4.4.8/fotorama.js"></script> 
<link rel="icon" href="images/BikeFavicon.png" type="image/x-icon">
</head>
<div id="container">
<body>
	<div id="navH">
		<a href ="home.php"><img id="logo" src="/images/boisestate-stackedlogo-black.jpg" alt="logo"></a>
			<div id="nav">
				<ul class="navUl">
					<li class="navLi"><a  href ="home.php">Home</a></li>
					<li><a href ="nutrition.php">Nutrition</a></li>
					<li><a href ="training.php">Training</a></li>
					<li><a href ="about.php">About</a></li>
					<li><a id="resultHover" href ="results.php">Results</a></li>
					<li><a id="loginHover" href ="login.php">Login</a></li>
				</ul>
		</div >
	</div>

 <div id="loginFloat" class="smallFormFloat">
   <fieldset>
       <legend>Login</legend>
       <form name="old_user" action="/handler/loginprocess.php" method="POST">
        <div class="pure-control-group"><label> Email:*</label> <input type="text" data-validation="email"  name="email"value =" <?php if(isset($_SESSION["email"])) echo $_SESSION["email"];?>"></div>
         <div class="pure-control-group"><label>Password:*</label> <input type="password" data-validation="required" name="password"></div>
       <div class="submit-control">
        <span id="newUserSpan" class="submitCopy">New user</span>
        <input type="submit" class="submit-button" name="existingButton" value="Login"></div>
         </form>
        </fieldset>
</div>
 <div id="newUserFloat" class="smallFormFloat">
   <fieldset>
      <legend>Sign-up</legend>
      <form name"new_user" action="/handler/userprocess.php" method="POST">
      <div class ="pure-control-group"><label>First Name:</label> <input type="text" data-validation="alphanumberic"  name= "first_name" value ="<?php if(isset($_SESSION["firstname"])) echo $_SESSION["firstname"];?>"></div>
       <div class ="pure-control-group"><label>Last Name: </label> <input type="text" data-validation="alpanumberic"  name= "last_name" value ="<?php if(isset($_SESSION["lastname"])) echo $_SESSION["lastname"];?>"></div>
            <div class ="pure-control-group"><label>Email: </label> <input type="text" data-validation="email"  name= "email" value ="<?php if(isset($_SESSION["email"])) echo $_SESSION["email"];?>"></div>
            <div class ="pure-control-group"><label>Password: </label> <input type="password" data-validation="length" data-validation-length="min8" name="password" ></div>
            <div class ="pure-control-group"><label>Confirm Password: </label> <input type="password" data-validation="length" data-validation-length="min8" name="confirm_password"></div>
           <div class="submit-control"> <input type="submit"class="submit-button"  name="newUserButton" value="submit"/></div>
   </fieldset>
</form>
</div>




<script>
$("#loginFloat").hide();
$("#newUserFloat").hide();
$("#newUserSpan").click(function(){
   $("#loginFloat").fadeOut();
   $("#newUserFloat").fadeIn();
});
$("#loginHover").mouseover(function(){$("#loginFloat").fadeIn();});
$("#loginFloat").mouseover(function(){$("#loginFloat").show();});
$("#resultHover").mouseover(function(){$("#loginFloat").fadeOut();});
$("#loginFloat").mouseleave(function(){$("#loginFloat").fadeOut();});
$("#newUserFloat").mouseleave(function(){$("#newUserFloat").fadeOut();});
</script>

	<div id="body">	
	
