<?php
//Get session variables
session_start();

require_once "../php/dao.php";
require_once "../php/sanitize.php";

//Create Objets
$dao = new Dao();
$sanitize= new Sanitize();

//Define variables
$fnameErr = $lnameErr = $emailErr =$telephoneErr =$addressErr =$cityErr=$stateErr=$zipErr=$countryErr="";
$firstname= $lastname= $email=$telephone=$address =$city=$state=$zip=$country="";
$genderErr= $shirtErr= $birthdayErr= $pass =$cpass= "";
$gender= $shirt= $birthday= "";
$emergency_nameErr= $emergency_telephoneErr= $relationshipErr= "";
$emergency_name= $emergency_telephone= $relationship= "";

$user = $dao->getUser($_SESSION["id"]);

if(isset($_POST["save_info"])){
   if(empty($_POST["First_Name"]))
      ($fnameErr = "First Name in Required");
   else
   {
      $firstname=$sanitize->cleanInput($_POST["First_Name"]);
      if(!preg_match("/^[a-zA-Z]*$/",$firstname)){
         $fnameErr= "No numbers or special characters ";
      }
   }
   if(empty($_POST["Last_Name"]))
         ($lnameErr= "Last Name Required");
      else
      {
         $lastname=$sanitize->cleanInput($_POST["Last_Name"]);
         if(!preg_match("/^[a-zA-Z]*$/",$lastname)){
            $lnameErr= " Nice try sneaky sneaky, I kill you, and No numbers or special charachers";
         }
      }

   if(empty($_POST["telephone"]))
         ($telephoneErr= "Telephone Required");
      else
      {
         $telephone=$sanitize->cleanInput($_POST["telephone"]);
         if(!preg_match("/[\d][\d][\d][\d][\d][\d][\d][\d][\d][\d]/",$telephone)){
            $telephoneErr= "Phone 10 digits and no special characters";
         }
         if(strlen($telephone) != 10)
            $telephoneErr= "Phone 10 digits and no special characters";
      }

 if(empty($_POST["Street"]))
         ($addressErr= "Street Address Required");
      else
      {
         $address=$sanitize->cleanInput($_POST["Street"]);
         if(preg_match("/\;/",$address)){
            $addressErr= " This is not a valid address";
         }
      } 
   
if(empty($_POST["email"]))
      ($emailErr= "Email Required");
   else
   {
      $email=$sanitize->cleanInput($_POST["email"]);
      if(!preg_match("/\w*\@(boisestate|u.boisestate)+(.edu)/",$email))
         $emailErr= "You must be a Boise State Student";
   }

 if(empty($_POST["City"]))
         ($cityErr= "Cit Required");
      else
      {
         $city=$sanitize->cleanInput($_POST["City"]);
         if(!preg_match("/[a-zA-Z]/",$city)){
            $cityErr= "Cities can only contain letters";
         }
      } 

  if(empty($_POST["State"]))
         ($stateErr= "State Required");
      else
      {
         $state=$sanitize->cleanInput($_POST["State"]);
         if(!preg_match("/(A[LKSZR]|C[AOT]|D[EC]|FL|GA|HI|I[DLNA]|K[SY]|LA|M[EHDAINSOT]|N[EVHJMYCD]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[TIA]|W[AVIY])/",strtoupper($state))){
            $stateErr= "Two letter abreviation";
         }
         if(strlen($state) != 2)
            $stateErr="Two letter abreviation";
      } 

  if(empty($_POST["Zip"])){
     echo $_POST["Zip"];
         ($zipErr= "Zip Required");}
      else
      {
         $zip=$sanitize->cleanInput($_POST["Zip"]);
         if(!preg_match("/[\d][\d][\d][\d][\d]/",$zip)){
            $zipErr= "Only digits please";
         }
         if(strlen($zip) != 5)
            $zipErr ="Only 5 digits please";
      } 

  if(empty($_POST["Country"]))
         ($countryErr= "Country Required");
      else
      {
         $country=$sanitize->cleanInput($_POST["Country"]);
         if(preg_match("/\;/",$country)){
            $countryErr= "Remove the colon please";
         }
      } 
 
  if($_POST["sex"] == "M" || $_POST["sex"]== "F")
         $gender = $_POST["sex"];
      else
         ($genderErr= "Gender Required");

  if(empty($_POST["shirt_size"]))
         ($shirtErr= "Shirt size Required");
      else
      {
         $shirt= $_POST["shirt_size"];
      } 

  if(empty($_POST["birthday"]))
         ($birthdayErr= "Birthday Required");
      else
      {
         $birthday= $_POST["birthday"];
      } 

   if(empty($_POST["emergency_name"]))
         ($emergency_nameErr= "emerency name Required");
      else
      {
         $emergency_name=$sanitize->cleanInput($_POST["emergency_name"]);
         if(!preg_match("/\;/",$emergency_name)){
            $Err= "Remove the colon please";
         }
      } 
    if(empty($_POST["emergency_telephone"]))
         ($emergency_telephoneErr= "emeregency telly Required");
      else
      {
         $emergency_telephone=$sanitize->cleanInput($_POST["emergency_telephone"]);
         if(!preg_match("/[\d][\d][\d][\d][\d][\d][\d][\d][\d][\d]/",$emergency_telephone)){
            $emergency_telephoneErr= "Phone Nice try sneaky sneaky, I kill you, and No numbers or special charachers";
         }
         if(strlen($emergency_telephone) !=10)
            $emergency_telephoneErr= "Ten DIGITS only";


      }
 if(empty($_POST["emergency_relationship"]))
         ($relationshipErr= "Relationship  Required");
      else
      {
         $relationship= $_POST["emergency_relationship"];
      } 
 
   if( $emergency_nameErr== $emergency_telephoneErr && 
      $relationshipErr== $genderErr && $shirtErr== $birthdayErri && 
      $fnameErr == $lnameErr && $emailErr ==$telephoneErr && 
      $addressErr ==$cityErr && $stateErr ==$zipErr &&$countryErr==""
   ){

      try{

         /*
          */

         session_start();
               $_SESSION["telephone"]= $telephone;
               $_SESSION["address"]= $address;
               $_SESSION["city"]= $city;
               $_SESSION["state"]= $state;
               $_SESSION["zip"]= $zip;
               $_SESSION["country"]= $country;
               $_SESSION["gender"]= $gender;
               $_SESSION["shirt"]= $shirt;
               $_SESSION["birthday"]= $birthday;
               $_SESSION["emergency_name"]= $emergency_name;
               $_SESSION["emergency_telephone"]= $emergency_telephone;
               $_SESSION["relationship"]= $relationship;
               $_SESSION["pass"]= $pass;
               $_SESSION["cpass"]= $cpass;

               $_SESSION["firstnameErr"]= $fnameErr;
               $_SESSION["lastnameErr"]= $lnameErr;
               $_SESSION["telephoneErr"]= $telephoneErr;
               $_SESSION["addressErr"]= $addressErr;
               $_SESSION["cityErr"]= $cityErr;
               $_SESSION["stateErr"]= $stateErr;
               $_SESSION["zipErr"]= $zipErr;
               $_SESSION["countryErr"]= $countryErr;
               $_SESSION["genderErr"]= $genderErr;
               $_SESSION["shirtErr"]= $shirtErr;
               $_SESSION["birthdayErr"]= $birthdayErr;
               $_SESSION["emergency_nameErr"]= $emergency_nameErr;
               $_SESSION["emergency_telephoneErr"]= $emergency_telephoneErr;
               $_SESSION["relationshipErr"]= $relationshipErr;
               $_SESSION["passErr"]= $passErr;
               $_SESSION["cpassErr"]= $cpassErr;
 
 
       
         $dao->userProfile($_SESSION["id"], $firstname, $lastname, $email, $telephone,
            $address, $city, $state, $zip, $country, $gender, $shirt, 
            $birthday,$emergency_name, $emergency_telephone, $relationship);
         header("Location: ../information.php");
      }catch(Exception $e){
         var_dump($e);}
   }
 else{
               $_SESSION["telephone"]= $telephone;
               $_SESSION["address"]= $address;
               $_SESSION["city"]= $city;
               $_SESSION["state"]= $state;
               $_SESSION["zip"]= $zip;
               $_SESSION["country"]= $country;
               $_SESSION["gender"]= $gender;
               $_SESSION["shirt"]= $shirt;
               $_SESSION["birthday"]= $birthday;
               $_SESSION["emergency_name"]= $emergency_name;
               $_SESSION["emergency_telephone"]= $emergency_telephone;
               $_SESSION["relationship"]= $relationship;
               $_SESSION["pass"]= $pass;
               $_SESSION["cpass"]= $cpass;
 
  

               $_SESSION["firstnameErr"]= $fnameErr;
               $_SESSION["lastnameErr"]= $lnameErr;
               $_SESSION["telephoneErr"]= $telephoneErr;
               $_SESSION["addressErr"]= $addressErr;
               $_SESSION["cityErr"]= $cityErr;
               $_SESSION["stateErr"]= $stateErr;
               $_SESSION["zipErr"]= $zipErr;
               $_SESSION["countryErr"]= $countryErr;
               $_SESSION["genderErr"]= $genderErr;
               $_SESSION["shirtErr"]= $shirtErr;
               $_SESSION["birthdayErr"]= $birthdayErr;
               $_SESSION["emergency_nameErr"]= $emergency_nameErr;
               $_SESSION["emergency_telephoneErr"]= $emergency_telephoneErr;
               $_SESSION["relationshipErr"]= $relationshipErr;
               $_SESSION["passErr"]= $passErr;
               $_SESSION["cpassErr"]= $cpassErr;
               header("Location: ../information.php");
 
 }
}

?>
