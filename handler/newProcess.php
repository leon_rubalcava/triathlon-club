<?php 
//see if the form has been submitted

session_start();
require_once"../php/dao.php";
require_once"../php/sanitize.php";
$sanitize= new Sanitize();

// This may look cleaner with switch cases

   if(isset($_POST["newUserButton"])){

      $first_name = $_POST ["first_name"];
      $last_name = $_POST ["last_name"];
      $email= $_POST ["email"];
      $password= $_POST ["password"];
      $password_confirm= $_POST ["confirm_password"];
      //Default is zero admin are one
      $permission = 0;
      //If password and password confirm don't match notify user
      //should check for email here instead of in dao
      try{
         $dao = new Dao();
         $dao->createUser($first_name, $last_name, $email, $password,$permission);
         header("Location:login.php");
      }catch(Exception $e){
         var_dump($e);}
   }

   if(isset($_POST["existingButton"])){
      $email= $_POST ["email"];
      $password= $_POST ["password"];
      try{
         $dao = new Dao();
         $bool = $dao->hasPermission($email, $password);
         if(isset($bool)){
            session_start();
            //print_r($bool);
            $_SESSION["name"]= $bool["first_name"];
            $_SESSION["id"]= $bool["id"];
            $_SESSION["permission"]=$bool["permissions"];
            header("Location:user_home.php");}

      }catch(Exception $e){
         var_dump($e);}
   }

if(isset($_POST["userProfile"])){

      $first_name = $_POST ["first_name"];
      $last_name = $_POST ["last_name"];
      $email= $_POST ["email"];
      $password= $_POST ["password"];
      $password_confirm= $_POST ["confirm_password"];
      //If password and password confirm don't match notify user
      //should check for email here instead of in dao
      try{
         $dao = new Dao();
         $dao->createUser($first_name, $last_name, $email, $password);
         header("Location:login.php");
      }catch(Exception $e){
         var_dump($e);}
   }

if(isset($_POST["check_submit"])){ 

      $title=$sanitize->cleanInput($_POST ["Title"]);
      $article=$sanitize->cleanInput($_POST ["Article"]);

      $type=$_POST["check_submit"];
      //If password and password confirm don't match notify user
      //should check for email here instead of in dao
      try{
         $dao = new Dao();
         $dao->saveArticle($title, $article, $type);
         //header("Location: ../admin.php");
      }catch(Exception $e){
         var_dump($e);}
   }

 if(isset($_POST["run_workout"])|| isset($_POST["bike_workout"]) || isset($_POST["swim_workout"])){ 

      $time= $_POST ["time"];
      $distance= $_POST ["distance"];
      $type=$_POST["workout"];

      if(!preg_match("/[\d]/",$time . $distance)){
         $_SESSION["success"]="";
         $_SESSION["error"]="Error, Only digits allowed";
         header("Location:../user_home.php");
      }
      else{
      //If password and password confirm don't match notify user
      //should check for email here instead of in dao
      try{
         $dao = new Dao();
         $dao->saveWorkout($_SESSION["id"], $time, $distance, $type);
         $_SESSION["error"]="";
         $_SESSION["success"]="Success";
         header("Location:../user_home.php");
      }catch(Exception $e){
         var_dump($e);}
      }
   }
 

 if(isset($_POST["result_submit"])){ 

      $runtime= $_POST["run_time"];
      $rundistance= $_POST["run_distance"];
      $biketime= $_POST ["bike_time"];
      $bikedistance= $_POST ["bike_distance"];
      $swimtime= $_POST ["swim_time"];
      $swimdistance= $_POST ["swim_distance"];
      $racename=$_POST["race_name"];

      if(!preg_match("/[\d]/", $runtime . $rundistance. $biketime . $bikedistance. $swimtime .$swimdistance)){
         $_SESSION["success"]="";
         $_SESSION["error"]="Error, Only digits allowed time and distance and no special charaters in race name";
         header("Location:../user_results.php");
      }
      else{
      //If password and password confirm don't match notify user
      //should check for email here instead of in dao
      try{
         $dao = new Dao();
         $dao->saveResult($_SESSION["id"], $runtime, $rundistance, $biketime, $bikedistance, $swimtime, $swimdistance, $racename);
         $_SESSION["error"]="";
         $_SESSION["success"]="Success";
         header("Location:../user_results.php");
      }catch(Exception $e){
         var_dump($e);}
      }
   }

   // This updates about blob for specific user
   if(isset($_POST["about_submit"])){ 
         $article= $sanitize->cleanInput($_POST ["about_article"]);
         try{
            $dao = new Dao();
            $dao->saveAbout($_SESSION["id"], $article);
            header("Location:../user_about.php");
         }catch(Exception $e){
            var_dump($e);}
      }
 
?>
