<?php
//Get session variables
session_start();

require_once "../php/dao.php";
require_once "../php/sanitize.php";

//Create Objets
$dao = new Dao();
$sanitize= new Sanitize();

//Define variables
$fnameErr = $lnameErr = $emailErr =$passErr =$cpassErr ="";
$firstname= $lastname= $email=$pass=$cpass="";

if(isset($_POST["newUserButton"])){
   if(empty($_POST["first_name"]))
      ($fnameErr = "First Name Required");
   else
   {
      $firstname=$sanitize->cleanInput($_POST["first_name"]);
      if(!preg_match("/^[a-zA-Z]*$/",$firstname)){
         $fnameErr= "No numbers or special characters ";
      }
   }
if(empty($_POST["last_name"]))
      ($lnameErr= "Last Name Required");
   else
   {
      $lastname=$sanitize->cleanInput($_POST["last_name"]);
      if(!preg_match("/^[a-zA-Z]*$/",$lastname)){
         $lnameErr= " Nice try sneaky sneaky, I kill you, and No numbers or special charachers";
      }
   }

if(empty($_POST["email"]))
      ($emailErr= "Email Required");
   else
   {
      $email=$sanitize->cleanInput($_POST["email"]);
      if(!preg_match("/\w*\@(boisestate|u.boisestate)+(.edu)/",$email)){
         $emailErr= "You must be a Boise State Student";
      }
      if($dao->userExists($email)!= null){
         $emailErr ="User already in database BeepBeepBoopBoop";
      }
   }

if(empty($_POST["password"]))
      ($passErr= "Password Required");
   else
   {
      $pass=$_POST["password"];
      if(strlen($pass) <7)
         $passErr="Password must be longer than 8 Characters";
   }

if(empty($_POST["confirm_password"]))
      ($cpassErr= "Must Confirm Password Required");
   else
   {
      $cpass=$_POST["confirm_password"];
      }

if($pass !== $cpass){
   $cpassErr="Passwords do not Match";
   }

 if(isset($_POST["newUserButton"]) && $fnameErr=="" && $lnameErr=="" && $emailErr==""&& $passErr==""){

      $permission = 0;
      try{
         $dao->createUser($firstname, $lastname, $email, $pass,$permission);
         $bool = $dao->hasPermission($email, $pass);
         session_start();
         $_SESSION["permission"] = $permission;
         $_SESSION["name"] = $firstname;
         $_SESSION["id"] = $bool["id"];
         header("Location: ../user_home.php");
      }catch(Exception $e){
         var_dump($e);}
   }
 else{
  
               $_SESSION["email"]= $email;
               $_SESSION["firstname"]= $firstname;
               $_SESSION["lastname"]= $lastname;
               $_SESSION["fnameErr"]= $fnameErr;
               $_SESSION["lnameErr"]= $lnameErr;
               $_SESSION["emailErr"]= $emailErr;
               $_SESSION["passErr"]= $passErr;
               $_SESSION["cpassErr"]= $cpassErr;
               header("Location: ../newUserLogin.php");
 
 }
}

?>
