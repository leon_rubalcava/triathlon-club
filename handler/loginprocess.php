<?php
//Get session variables
session_start();
require_once "../php/sanitize.php";
require_once "../php/dao.php";

//Create Objects
$dao = new Dao();
$sanitize= new Sanitize();

//Define variables
$emailErr =$passErr ="";
$email=$pass="";

if(isset($_POST["existingButton"])){
   if(empty($_POST["email"]))
      ($emailErr= "Email Required");
   else
   {
      $email=$sanitize->cleanInput($_POST["email"]);
      if(!preg_match("/\w*\@(boisestate|u.boisestate)+(.edu)/",$email)){
         $emailErr= "No Queso for YOU";
      }
   }

   if(empty($_POST["password"]))
      ($passErr= "Password Required");
   else
   {
      $pass=$_POST["password"];
   }
      
  try{
         $bool = $dao->hasPermission($email, $pass);
       }catch(Exception $e){var_dump($e);}

   if(isset($_POST["existingButton"]) && $passErr=="" && $emailErr=="" && !empty($bool)){
               $_SESSION["name"]= $bool["first_name"];
               $_SESSION["id"]= $bool["id"];
               $_SESSION["permission"]=$bool["permissions"];
               header("Location: ../user_home.php");}
   else{
               $passErr = "Incorrect username or password";
               $_SESSION["email"] = $email;
               $_SESSION["emailErr"]= $emailErr;
               $_SESSION["passErr"]= $passErr;
               header("Location: ../login.php");
      }
}

?>
