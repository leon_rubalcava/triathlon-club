<?php 
//Gett session data
session_start();

//Require Files
require_once"php/permission.php";
require_once"php/render.php";

//Objects
$render = new Render();
$permission= new Permission();

//Check user permissions
$permission->userOnly();

//Render page
$signout = (isset($_SESSION["permission"])) ?"Sign out": "";
include ('header.php');
$render->openPage(ucfirst($_SESSION["name"]). "'s Results"   , $signout);

/*
 *Menu items are in an associative array of the form
 *$displayedText => Url
 *
 */
$menuOptions =array(
   "Information"=>"information.php",
   "About"=>"user_about.php",
   "Results"=>"user_results.php");
if($_SESSION["permission"] ==1)
   $menuOptions[Admin]="admin.php";
$render->sideMenuArray($menuOptions); 
?>
 <div id ="results" class="athlete_profile" >  
 <form  name="results"  action="handler/newProcess.php" method = "POST">
 <fieldset>
 <legend>Enter your last race info</legend>
 <div class ="pure-control-group"><label for="run_distance">Run Distance:</label> <input type="text" data-validation="number" data-validation-allowing="float" name="run_distance"></div>
 <div class ="pure-control-group"><label for="run_time">Run Time:</label> <input type="text" data-validation="time" name="run_time"></div>
 <div class ="pure-control-group"><label for="bike_distance">Bike Distance:</label> <input type="text" data-validation="number" data-validation-allowing="float"name="bike_distance"></div>
 <div class ="pure-control-group"><label for="bike_time">Bike Time:</label> <input type="text" data-validation="time"name="bike_time"></div>
 <div class ="pure-control-group"><label for="swim_distance">Swim Distance:</label> <input type="text" data-validation="number" data-validation-allowing="float"name="swim_distance"></div>
 <div class ="pure-control-group"><label for="swim_time">Swim Time:</label> <input type="text" data-validation="time" name="swim_time"></div>
 <div class ="pure-control-group"><label for="race_name">Race Name:</label> <input type="text" data-validation="custom " data-validation-regexp="^([a-zA-Z ]+$)"name="race_name"></div>
                  <input type="submit" class ="submit-button" name="result_submit" value="Save"/>
</fieldset>
</form>

<div id="success" class="error">
<?php echo $_SESSION["success"] .  $_SESSION["error"];?>
</div>
</div>
<div id="clear">  </div>



<?php $_SESSION["success"]="";
 $_SESSION["error"]=""; 
 include ('footer.php');?>
