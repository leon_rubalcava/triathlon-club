<?php 
//Start a new session or retrieve the existing one
session_start();

//Require php files
require_once"php/dao.php";
require_once"php/render.php";

//Create required objects
$dao = new Dao();
$render = new Render();

//Render page contents
include ('header.php');
$signout = (isset($_SESSION["permission"])) ?"Sign out": "";
$render->openPage("Nutrition",$signout);

//Populate Side menu
$Titles = $dao->articleTitles("nutrition_article");
$render->sideMenu($Titles, "nutrition");
                   
//Display blog content
$displayedTitle=$_GET["article"];
$titleArticle=$dao->getArt("nutrition_article",$displayedTitle); 

if($displayTitle =="" && $titleArticle ==""){
   $render->blogDiv("<------Use the Links over here find nutrition articles", $titleArticle["article"]);
}
else{$render->blogDiv($titleArticle["title"], $titleArticle["article"]);}
?>

<?php include ('footer.php');?>
