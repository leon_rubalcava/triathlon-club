<?php 
//Gett session data
session_start();

//Require Files
require_once"php/permission.php";
require_once"php/render.php";

//Objects
$render = new Render();
$permission= new Permission();

//Check user permissions
$permission->userOnly();

//Render page
$signout = (isset($_SESSION["permission"])) ?"Sign out": "";
include ('header.php');
$render->openPage(ucfirst($_SESSION["name"])."'s About", $signout);

/*
 *Menu items are in an associative array of the form
 *$displayedText => Url
 *
 */
   

$menuOptions =array(
   "Information"=>"information.php",
   "About"=>"user_about.php",
   "Results"=>"user_results.php");

if($_SESSION["permission"] ==1)
   $menuOptions[Admin]="admin.php";
$render->sideMenuArray($menuOptions); 
?>

 <div id="about" class ="athlete_profile">  
 <form  name="about" action="handler/newProcess.php" method = "POST">
               <fieldset>
                  <legend>About you</legend>
      
                  This will be viewable on About page<textarea rows="5" cols="80" data-validation="length" data-validation-length="min100" name = "about_article"></textarea>
                  <input type="submit" class="submit-button" name="about_submit" value="Save"/>
</fieldset>
</form>



</div> 
  <div id="clear">  </div>



<?php include ('footer.php');?>
