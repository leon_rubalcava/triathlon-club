<?php 
//Start new Session
session_start();

//reqire files
require_once"php/render.php";
require_once"php/dao.php";

//Objects
$render = new Render();
$dao = new Dao();

//Render top of page
$signout = (isset($_SESSION["permission"])) ?"Sign out": "";
include ('header.php');
$render->openPage("About",$signout);


//Populate Side menu
$Athletes= $dao->getAbout();
$menuOptions =array();
foreach($Athletes as $athlete){
  $menuOptions[$athlete["first_name"]] = "#" . $athlete["first_name"];
}
$render->sideMenuArray($menuOptions);

//Populate the about section
$Athletes= $dao->getAbout();
foreach($Athletes as $athlete){
$render->blogDiv($athlete["first_name"], $athlete["about"]);
}

include ('footer.php');?>
