<?php //Start new Session
session_start();

//Require files
require_once "php/render.php";

//create objects
$render = new Render();

//Render top of  page
include ('header.php');
$render->openPage("New User Registration", "");

//Set session var's to null

?>

<div id="login_form">
<div id="newUser" class="smallForm">
   <fieldset>
      <legend>Sign-up</legend>
      <form name"new_user" action="/handler/userprocess.php" method="POST">
      <div class ="pure-control-group"><label>First Name:</label> <input type="text" data-validation="alphanumberic"  name= "first_name" value ="<?php if(isset($_SESSION["firstname"])) echo $_SESSION["firstname"];?>"></div>
       <div class ="pure-control-group"><label>Last Name: </label> <input type="text" data-validation="alpanumberic"  name= "last_name" value ="<?php if(isset($_SESSION["lastname"])) echo $_SESSION["lastname"];?>"></div>
            <div class ="pure-control-group"><label>Email: </label> <input type="text" data-validation="email"  name= "email" value ="<?php if(isset($_SESSION["email"])) echo $_SESSION["email"];?>"></div>
            <div class ="pure-control-group"><label>Password: </label> <input type="password" data-validation="length" data-validation-length="min8" name="password" ></div>
            <div class ="pure-control-group"><label>Confirm Password: </label> <input type="password" data-validation="length" data-validation-length="min8" name="confirm_password"></div>
           <div class="submit-control"> <input type="submit"class="submit-button"  name="newUserButton" value="submit"/></div>
   </fieldset>
</form>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.27/jquery.form-validator.min.js"></script>
<script> $.validate();
</script>
</div>
<div id="login_error">
<span class="error-php"><?php if(isset($_SESSION["emailErr"])){echo $_SESSION["emailErr"];}?><span><br>
<span class="error-php"><?php if(isset($_SESSION["passErr"])){echo $_SESSION["passErr"];}?><span><br>
<span class="error-php"><?php if(isset($_SESSION["fnameErr"])){echo $_SESSION["fnameErr"];}?><span><br>
<span class="error-php"><?php if(isset($_SESSION["lnameErr"])){echo $_SESSION["lnameErr"];}?><span><br>
<span class="error-php"><?php if(isset($_SESSION["cpassErr"])){echo $_SESSION["cpassErr"];}?><span><br>
</div>

</div>
 <?php
$_SESSION["emailErr"] ="";
$_SESSION["passErr"] ="";
 include ('footer.php');?>
