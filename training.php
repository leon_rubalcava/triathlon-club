<?php
//Start a new session or retrieve existing one
session_start();

//Require php files
require_once"php/render.php";
require_once"php/dao.php";

//Create required objects
$dao = new Dao();
$render = new Render();


//
/**Header and Signout*/
include ('header.php');
$signout = (isset($_SESSION["permission"])) ?"Sign out": "";
$render->openPage("Training",$signout);

//Populate side menu
$Titles = $dao->articleTitles("training_article");
$render->sideMenu($Titles, "training");
   

//Display blog content
$displayedTitle=$_GET["article"];
$titleArticle=$dao->getArt("training_article",$displayedTitle);

if($displayTitle =="" && $titleArticle ==""){
   $render->blogDiv("<------Use the Links over here find Training articles", $titleArticle["article"]);
}
else{$render->blogDiv($titleArticle["title"], $titleArticle["article"]);}

include ('footer.php');?>
