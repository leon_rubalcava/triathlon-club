<?php 
//Start new session
session_start();

//require fiels
require_once"php/dao.php";
require_once"php/render.php";

//Objects
$render = new render();
$dao = new Dao();

$signout = (isset($_SESSION["permission"])) ?"Sign out": "";
include ('header.php');
$render->openPage("Results", $signout);

$Races= $dao->getRaces();
$menuOptions =array();
foreach($Races as $race){
  $menuOptions[$race["race_name"]] = "#" . $race["race_name"];
}
$render->sideMenuArray($menuOptions);

$Races= $dao->getRaces();
foreach($Races as $race){
   $render->blogDiv($race["race_name"],
      "Results for ". $race["first_name"]."<br> Run Distance=" . $race["run_distance"] . " <br> Run Time=".$race["run_time"] .
      "<br>Bike Distance=" . $race["bike_distance"] . "<br> Bike Time=".$race["bike_time"] .
      "<br>Swim Distance=" . $race["swim_distance"] . "<br> Swim Time=".$race["swim_time"]
   );
}
?>







<?php include ('footer.php');?>
